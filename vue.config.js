const CompressionPlugin = require('compress-webpack-plugin')
module.exports = {
    baseUrl:'/',
    outputDir:'dist',
    assetsDir:'static',
    lintOnSave:false,
    productionSourceMap:false,
    configureWebpack:config => {
        if(process.env.NODE_ENV === 'production'){
            return {
                plugins:[
                    new CompressionPlugin({
                        test:/\.js$/,
                        asset: "[path].gz[query]",
                        // threshold:10240,
                        deleteOriginalAssets:false
                    })
                ]
            }
        }
    }
}