const mixin = {
    methods:{
        convertTime(time){
            let date = new Date(time)
            let dateStr = date.toLocaleDateString().replace(/\d+/g, n => n.padStart(2, '0')).replace(/\//g, '-')
            let timeStr = `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`
            return `${dateStr} ${timeStr}`
        },
        getToken(){
            const token = localStorage.getItem("token")
            return token
        }
    },
    computed:{
        purchase(){
            return this.$store.state.match.purchase
        }
    },
    filters:{
        convertTime(time){
            if(!time){
                return ""
            }
            let date = new Date(time)
            let dateStr = date.toLocaleDateString().replace(/\d+/g, n => n.padStart(2, '0')).replace(/\//g, '-')
            let timeStr = `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`
            return `${dateStr} ${timeStr}`
        },
        convertTimeWithSec(time){
            if(!time){
                return ""
            }
            let date = new Date(time)
            let dateStr = date.toLocaleDateString().replace(/\d+/g, n => n.padStart(2, '0')).replace(/\//g, '-')
            let timeStr = `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`
            return `${dateStr} ${timeStr}`
        },
        convertDate(str){
            return str.split(' ')[0]
        },
        toPercentage(num){
            return (num*100).toFixed(2) + '%'
        },
    }
}
export default mixin