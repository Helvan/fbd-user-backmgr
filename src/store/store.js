import Vue from "vue";
import Vuex from "vuex";
import match from "./match"
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    account: localStorage.getItem("fbd_usr_ccount"),
    showAside: false,
    hasLogin: false,
    amount:0
  },
  mutations: {
    getAccount(state, payload) {
      state.account = payload;
    },
    setAside(state, payload) {
      state.showAside = payload;
    },
    setLogin(state, payload) {
      state.hasLogin = payload;
    },
    updateAmount(state, {amount}){
      state.amount = amount
    }
  },
  actions:{
    updateAmount({commit, state}){
      const token = localStorage.fbd_usr_token
      if(token){
        Vue.prototype.$http.get(Vue.prototype.$api.userbalance, {
          headers:{token}
        }).then(res => {
          const amount = res.amount
          commit("updateAmount", {amount})
        }).catch(err => {
          if(err.data && err.data.code === -40004){
              localStorage.removeItem("fbd_usr_token");
              localStorage.removeItem("fbd_usr_ccount");
              commit("setAside", false);
              commit("setLogin", false);
          }
        })
      }
    }
  },
  modules:{
    match
  }
});

export default store;
