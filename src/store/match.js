let orders = localStorage.getItem("orders")
if(orders){
    orders = JSON.parse(orders)
}else{
    orders = []
}
const state = {
    orders:orders,
    purchase:0
}
const getters = {
    amount:state => {
        localStorage.setItem('orders', JSON.stringify(state.orders))
        let count = state.orders.reduce((sum, order) => sum += Number(order.amount), 0)
        return count
    },
    orderList: state => {
        const orders = state.orders
        const list = orders.reduce((result, order) => {
            const tmp = result.find(v => v.raceId === order.raceId)
            if(tmp === undefined){
                const {raceId, category, homeTeam, visitTeam, startTime} = order
                const obj = {
                    raceId,
                    category,
                    homeTeam,
                    visitTeam,
                    startTime,
                    list:[order]
                }
                result.push(obj)
            }else{
                tmp.list.push(order)
            }
            return result
        }, [])
        return list
    },
    orderCount:(state, getters) => {
        return getters.orderList.reduce((rst, order) => rst += order.list.length, 0)
    },
    orderUpload:(state) => {
        const orders = state.orders
        console.log(orders)
        const result = orders.map(v => ({
            raceId:v.raceId,
            rebateId:v.rebateId,
            amount:v.amount,
            rebateRatio:v.ratio
        }))
        return result
    },
    preReward(state){
        const amount = state.orders.reduce((sum, order) => sum += Number(order.amount)*(Number(order.ratio) + 1), 0)
        return Number(amount).toFixed(2)
    }
}
const mutations = {
    addOrder(state, order){
        state.orders.push(order)
    },
    updateOrder(state, order){
        const index = state.orders.findIndex(v => v.raceId === order.raceId && v.rebateId === order.rebateId)
        if(index >= 0){
            state.orders.splice(index, 1, order)
        }
    },
    removeOrder(state, order){
        let index = state.orders.indexOf(order)
        state.orders.splice(index, 1)
    },
    clearList(state){
        state.orders = []
    },
    updateRatio(state, {rebateId, ratio}){
        const order = state.orders.find(v => v.rebateId = rebateId)
        order.ratio = ratio
    },
    purchaseIncrement(state){
        state.purchase += 1
    }
}
const actions = {
    addOrder({commit, state}, order){
        const orders = state.orders
        const index = orders.findIndex(v => v.raceId === order.raceId && v.rebateId === order.rebateId)
        const dist = orders[index]
        if(dist === undefined){
            commit("addOrder", order)
        }else{
            // let amount = Number(dist.amount) + Number(order.amount)
            // let ratio = order.ratio
            // let mergeOrder = Object.assign(order, {amount, ratio})
            // commit("updateOrder", mergeOrder)
            commit("updateOrder", order)
        }
    },
    removeOrder({commit, state}, {raceId, rebateId}){
        const order = state.orders.find(v => v.raceId === raceId&& v.rebateId === rebateId)
        if(order !== undefined){
            commit("removeOrder", order)  
        }
    },
    removeAllOrders({commit}){
        commit("clearList")
    },
    updateOrdersRatio({commit}, {unbets}){
        unbets.forEach(v => {
            const rebateId = v.id
            const ratio = v.ratio
            commit("updateRatio", {rebateId, ratio})
        })
    },
    countPurchase({commit}){
        commit("purchaseIncrement")
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}