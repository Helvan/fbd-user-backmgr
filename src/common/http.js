import axios from "axios";
import { Message, Loading } from "element-ui";
import Qs from "qs";

axios.defaults.baseURL = SERVER_PATH;

// let loading;
// function startLoading() {
//   loading = Loading.service({
//     lock: true,
//     text: "正在加载中……",
//     background: "rgba(0, 0, 0, 0.2)",
//     fullscreen: true
//   });
// }
// function endLoading() {
//   loading.close();
// }
// let needLoadingRequestCount = 0;

// export function showFullScreenLoading() {
//   if (needLoadingRequestCount === 0) {
//     startLoading();
//   }
//   needLoadingRequestCount++;
// }

// export function tryHideFullScreenLoading() {
//   if (needLoadingRequestCount <= 0) return;
//   needLoadingRequestCount--;
//   if (needLoadingRequestCount === 0) {
//     endLoading();
//   }
// }

const httpInstance = axios.create({
  responseType: "json",
  timeout: 10000,
  params: {},
  data: {},
  toQs: true, //是否需要序列化
  headers: {
    "Content-Type": "application/json",
  },

  validateStatus(status) {
    return status >= 200 && status < 300;
  }
});

function responseErrorHandler(error) {
  let text = "未知错误";
  const response = JSON.parse(JSON.stringify(error));
  if (response.data || response.response.data) {
    const { code, msg } = response.data || response.response.data;
    text = msg ? msg : text;
    // if (code === -40004) {
      // localStorage.removeItem("fbd_usr_token");
      // window.location.hash = "/";
    // }
  } else {
    text = "服务器响应失败";
  }
  Message.error(text);
}

httpInstance.interceptors.request.use(
  config => {
    const configs = config;
    if (localStorage.getItem("fbd_usr_token")) {
      configs.headers.token = localStorage.getItem("fbd_usr_token");
    }
    for (const key in configs.params) {
      if (configs.params[key] === "") {
        delete configs.params[key];
      }
    }
    if (configs.toQs) {
      configs.data = Qs.stringify(config.data);
    }
    // showFullScreenLoading();
    return configs;
  },
  error => Promise.reject(error)
);

httpInstance.interceptors.response.use(
  response => {
    const { status, data } = response;
    const { code, result } = data;
    // tryHideFullScreenLoading();
    if (status === 200) {
      if (code === 200) {
        if (result instanceof Array) {
          if (!result[0]) {
            return { nodata: true };
          } else {
            return result;
          }
        } else {
          // let arr = Object.keys(result);
          if (result) {
            return result;
          } else {
            return {};
          }
        }
      }
      responseErrorHandler(response);
    }
    return Promise.reject(response);
  },
  error => {
    // tryHideFullScreenLoading();
    responseErrorHandler(error);
    return Promise.reject(error);
  }
);

export const $http = {
  get: (url, params = null, config = {}) => {
    const normalizedParams = params;
    return httpInstance.get(url, {
      params: normalizedParams,
      ...config
    });
  },
  post: (url, data = null, config = {}) => httpInstance.post(url, data, config)
};
export default {
  install(Vue) {
    Object.defineProperty(Vue.prototype, "$http", {
      value: $http,
      writable: false
    });
  }
};
