const api = {
  area: "api-user/user/open/phone/area",
  register: "api-user/user/open/register",
  captcha: "api-user/user/open/captcha",
  login: "api-user/user/open/login",
  logout: "api-user/user/logout",
  forgetPsw: "api-user/user/open/forget/password",
  uploadsnapshot:'api-tx/transaction/admin/upload/snapshot',
  betList:'api-bet/bet/list',
  uploadsnapshot:'api-tx/transaction/upload/snapshot',
  recharge:'api-tx/transaction/offline',
  withdraw:'api-tx/transaction/withdraw/submit',
  transactionList:'api-tx/transaction/list',

  category:'api-event/event/open/category',
  events:'api-event/event/open/eventDetailList',
  submitOrders:'api-bet/bet/add',
  userbalance:'api-tx/transaction/user/balance',
  recomend:'api-event/event/open/eventRecommendList',
  resultlist:'api-event/event/open/eventResultList',
  charge:'api-tx/transaction/charge',
  cancel:'api-bet/bet/cancellations'
};

export default {
  install(Vue) {
    Object.defineProperty(Vue.prototype, "$api", {
      value: api,
      writable: false
    });
  }
};
