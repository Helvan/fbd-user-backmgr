/**
 * 全局组件
 */
import Header from "@/components/header";
import Aside from "@/components/aside";

const globalComps = [Header, Aside];

export default {
  install(Vue) {
    globalComps.forEach(comp => {
      Vue.component(comp.name, comp);
    });
  }
};

