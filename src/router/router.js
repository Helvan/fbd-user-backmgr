const Index = () => import("@/views/Index");
const Register = () => import("@/views/register");
const ForgetPsw = () => import("@/views/forgetPsw");
const Home = () => import("@/views/home");
const MatchBet = () => import("@/views/matchBet");
const Recharge = () => import("@/views/cash/recharge");
const BetOrder = () => import("@/views/betOrder");
const Revise = () => import("@/views/revise");
const MatchTable = () => import("@/components/matchBets/matchTable")
const MatchDetail = () => import("@/components/matchBets/matchDetail")
const MatchResult = () => import("@/views/matchResult")
const CashHome = () => import("@/views/cash/home")

const mainMap = [
  {
    path: "/",
    name: "index",
    meta: { auth: true },
    component: Index
  },
  {
    path: "/register",
    name: "register",
    meta: { auth: true },
    component: Register
  },
  {
    path: "/forgetPsw",
    name: "forgetPsw",
    meta: { auth: false },
    component: ForgetPsw
  },
  {
    path: "/matchBet",
    meta: { auth: true },
    component: Home,
    children: [
      {
        path: "/",
        component: MatchBet,
        children:[{
          path:'',
          name:"matchBet",
          component:MatchTable,
        },{
          path: 'detail',
          name: 'matchDetail',
          component:MatchDetail
        }]
      }
    ]
  },{
    path: "/matchResult",
    meta:{auth:true},
    component:Home,
    name:"matchResult",
    children:[{
      path:"",
      component:MatchResult
    }]
  },
  {
    path: "/recharge",
    meta: { auth: true },
    component: Home,
    children: [
      {
        path: "/",
        component: CashHome
      }
    ]
  },
  {
    path: "/betOrder",
    meta: { auth: true },
    component: Home,
    children: [
      {
        path: "/",
        component: BetOrder
      }
    ]
  },
  {
    path: "/revise",
    meta: { auth: true },
    component: Home,
    children: [
      {
        path: "/",
        component: Revise
      }
    ]
  },
];

export default [...mainMap];
