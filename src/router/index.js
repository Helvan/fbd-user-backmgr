import Vue from "vue";
import Router from "vue-router";
import MainRouter from "./router";

Vue.use(Router);
const router = new Router({
   mode: "history",
  // base: process.env.BASE_URL,
  routes: [...MainRouter]
});
// 路由拦截
router.beforeEach((to, from, next) => {
  // console.log(to);
  if (localStorage.getItem("fbd_usr_token")) {
    next();
  } else {
    if (to.name === "index" || to.name === "register" || to.name === 'forgetPsw') {
      next();
    } else {
      alert("请先登录");
      next({ path: "/" });
    }
  }
});
export default router;
