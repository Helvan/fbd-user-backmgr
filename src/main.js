import Vue from "vue";
import App from "./App.vue";
import router from "./router/index";
import store from "./store/store";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import Http from "./common/http";
import Api from "./common/api";
import Components from "./common/component";
import mixin from "./mixins/mixins"
Vue.use(ElementUI);
Vue.use(Http);
Vue.use(Api);
Vue.use(Components);

Vue.config.productionTip = false;
Vue.mixin(mixin)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
